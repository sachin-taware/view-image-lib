package com.mavericklabs.imageviewersample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.mavericklabs.imageviewer.ImageViewer;
import com.mavericklabs.imageviewer.constants.IndicatorType;
import com.mavericklabs.imageviewer.models.ImageWithTitleModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void loadImages(View view) {
        new ImageViewer.Builder(this)
//                .urls(getUrls())
                .imagesWithTitles(getImagesWithTitles())
                .indicatorType(IndicatorType.LINE)
                .title(R.string.app_name)
                .isForSelect(true, null)
                .show();
    }

    private ArrayList<String> getUrls() {
        final ArrayList<String> imageUrlsList = new ArrayList<>();
        imageUrlsList.add("https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf");
        imageUrlsList.add("/storage/emulated/0/demo/vehicle_insurance.jpg");
        imageUrlsList.add("/storage/emulated/0/demo/driver_licence.pdf");
        imageUrlsList.add("https://upload.wikimedia.org/wikipedia/commons/3/3f/Francisco_de_Goya_y_Lucientes_-_Los_fusilamientos_del_tres_de_mayo_-_1814.jpg");
//        imageUrlsList.add("https://upload.wikimedia.org/wikipedia/commons/9/99/Las_Meninas_01.jpg");
//        imageUrlsList.add("https://upload.wikimedia.org/wikipedia/commons/f/f1/El_caballero_de_la_mano_en_el_pecho.jpg");
//        imageUrlsList.add("https://upload.wikimedia.org/wikipedia/commons/8/8b/Giovanni_Battista_Tiepolo_022.jpg");
//        imageUrlsList.add("https://www.alejandrodelasota.org/wp-content/uploads/2013/03/demoform1.pdf");
        return imageUrlsList;
    }

    private ArrayList<ImageWithTitleModel> getImagesWithTitles() {
        ArrayList<ImageWithTitleModel> models = new ArrayList<>();
        ImageWithTitleModel model1 = new ImageWithTitleModel(1L, "Image1", "https://upload.wikimedia.org/wikipedia/commons/3/3f/Francisco_de_Goya_y_Lucientes_-_Los_fusilamientos_del_tres_de_mayo_-_1814.jpg");
        ImageWithTitleModel model2 = new ImageWithTitleModel(2L, "Image2", "https://upload.wikimedia.org/wikipedia/commons/9/99/Las_Meninas_01.jpg");
        ImageWithTitleModel model3 = new ImageWithTitleModel(3L, "Image3", "https://upload.wikimedia.org/wikipedia/commons/f/f1/El_caballero_de_la_mano_en_el_pecho.jpg");
        models.add(model1);
        models.add(model2);
        models.add(model3);
        return models;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageViewer.VIEW_IMAGES_ACTIVITY && resultCode == RESULT_OK) {
            Log.e("VIEW_IMAGES_ACTIVITY", "CHANGE IMAGE");
            if (data != null && data.hasExtra(ImageViewer.SELECTED_IMAGE_ID))
                Log.e("Selected", "" + data.getLongExtra(ImageViewer.SELECTED_IMAGE_ID, 0));
        }
    }
}
