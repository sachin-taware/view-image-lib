package com.mavericklabs.imageviewer;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

import com.mavericklabs.imageviewer.constants.IndicatorType;
import com.mavericklabs.imageviewer.models.ImageWithTitleModel;

import java.util.ArrayList;

/**
 * Created by Prashant Kashetti on 17/12/18.
 */
public class ImageViewer {
    public static final int VIEW_IMAGES_ACTIVITY = 1000;
    public static final String SELECTED_IMAGE_ID = "selected_image_id";
    private ArrayList<String> urls;
    private IndicatorType indicatorType;
    private Integer placeHolderImage;
    private String title;
    private int titleId;
    private Activity activityContext;
    private Fragment fragmentContext;
    private boolean isForEdit;
    private boolean isForSelect;
    private Integer selectButtonName;
    private ArrayList<ImageWithTitleModel> imageWithTitleModels;

    private ImageViewer(Builder builder) {
        this.activityContext = builder.activityContext;
        this.fragmentContext = builder.fragmentContext;
        this.urls = builder.urls;
        this.indicatorType = builder.indicatorType;
        this.placeHolderImage = builder.placeHolderImage;
        this.title = builder.title;
        this.titleId = builder.titleId;
        this.isForEdit = builder.isForEdit;
        this.isForSelect = builder.isForSelect;
        this.selectButtonName = builder.selectButtonName;
        this.imageWithTitleModels = builder.imageWithTitleModels;
    }

    private void show() {
        if (urls == null || urls.size() == 0) {
            if (imageWithTitleModels == null || imageWithTitleModels.size() == 0)
                throw new IllegalArgumentException("Pass at least one URL");
        }

        if (activityContext == null && fragmentContext == null)
            throw new IllegalArgumentException("Pass activity context or fragment context");

        Intent intent;
        if (activityContext != null)
            intent = new Intent(activityContext, ViewImagesActivity.class);
        else
            intent = new Intent(fragmentContext.getContext(), ViewImagesActivity.class);

        if (urls != null && urls.size() > 0)
            intent.putExtra(ViewImagesActivity.IMAGE_URLS, urls);
        else
            intent.putParcelableArrayListExtra(ViewImagesActivity.IMAGE_WITH_TITLES, imageWithTitleModels);

        intent.putExtra(ViewImagesActivity.INDICATOR_TYPE, indicatorType);
        intent.putExtra(ViewImagesActivity.IS_FOR_EDIT, isForEdit);
        intent.putExtra(ViewImagesActivity.IS_FOR_SELECT, isForSelect);
        if (isForSelect && selectButtonName != null) {
            intent.putExtra(ViewImagesActivity.SELECT_BUTTON_NAME, selectButtonName);
        }
        if (title != null)
            intent.putExtra(ViewImagesActivity.TITLE, title);
        if (titleId != -1)
            intent.putExtra(ViewImagesActivity.TITLE_ID, titleId);
        if (placeHolderImage != null)
            intent.putExtra(ViewImagesActivity.PLACE_HOLDER_IMAGE, placeHolderImage);
        if (activityContext != null)
            activityContext.startActivityForResult(intent, VIEW_IMAGES_ACTIVITY);
        else
            fragmentContext.startActivityForResult(intent, VIEW_IMAGES_ACTIVITY);
    }

    public static class Builder {
        private Activity activityContext;
        private Fragment fragmentContext;
        private ArrayList<String> urls;
        private IndicatorType indicatorType;
        private Integer placeHolderImage;
        private String title;
        private int titleId = -1;
        private boolean isForEdit;
        private boolean isForSelect;
        private Integer selectButtonName;
        ArrayList<ImageWithTitleModel> imageWithTitleModels;

        public Builder(@NonNull Activity activityContext) {
            this.activityContext = activityContext;
            indicatorType = IndicatorType.CIRCLE;
        }

        public Builder(@NonNull Fragment fragmentContext) {
            this.fragmentContext = fragmentContext;
            indicatorType = IndicatorType.CIRCLE;
        }

        public Builder urls(ArrayList<String> urls) {
            this.urls = urls;
            return this;
        }

        public Builder indicatorType(IndicatorType indicatorType) {
            this.indicatorType = indicatorType;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder isForEdit(boolean isForEdit) {
            this.isForEdit = isForEdit;
            return this;
        }

        public Builder isForSelect(boolean isForSelect, @StringRes Integer selectButtonName) {
            this.isForSelect = isForSelect;
            this.selectButtonName = selectButtonName;
            return this;
        }

        public Builder title(@StringRes int titleId) {
            this.titleId = titleId;
            return this;
        }

        public Builder placeHolderImage(@DrawableRes Integer placeHolderImage) {
            this.placeHolderImage = placeHolderImage;
            return this;
        }

        public Builder imagesWithTitles(ArrayList<ImageWithTitleModel> imageWithTitleModels) {
            this.imageWithTitleModels = imageWithTitleModels;
            return this;
        }

        public void show() {
            ImageViewer viewer = new ImageViewer(this);
            viewer.show();
        }
    }
}
