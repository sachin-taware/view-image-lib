package com.mavericklabs.imageviewer.callbacks;

public interface OnSnapPositionChangeListener {
    void onSnapPositionChange(int position);
}
