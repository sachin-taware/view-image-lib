package com.mavericklabs.imageviewer.constants;

/**
 * Created by Prashant Kashetti on 17/12/18.
 */
public enum IndicatorType {
    LINE, CIRCLE
}
