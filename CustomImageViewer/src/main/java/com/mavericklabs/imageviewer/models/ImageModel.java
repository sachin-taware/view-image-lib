package com.mavericklabs.imageviewer.models;

import android.net.Uri;

/**
 * Created by Prashant Kashetti on 15/12/18.
 */
public class ImageModel {
    private Long id;
    private String title;
    private Uri uri;

    public ImageModel(Uri uri) {
        this.uri = uri;
    }

    public ImageModel(Long id, String title, Uri uri) {
        this.id = id;
        this.title = title;
        this.uri = uri;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
