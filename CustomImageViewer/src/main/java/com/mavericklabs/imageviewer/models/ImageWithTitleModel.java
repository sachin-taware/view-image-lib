package com.mavericklabs.imageviewer.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageWithTitleModel implements Parcelable {
    private Long id;
    private String title;
    private String url;

    public ImageWithTitleModel(Long id, String title, String url) {
        this.id = id;
        this.title = title;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeString(this.url);
    }

    protected ImageWithTitleModel(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.title = in.readString();
        this.url = in.readString();
    }

    public static final Creator<ImageWithTitleModel> CREATOR = new Creator<ImageWithTitleModel>() {
        @Override
        public ImageWithTitleModel createFromParcel(Parcel source) {
            return new ImageWithTitleModel(source);
        }

        @Override
        public ImageWithTitleModel[] newArray(int size) {
            return new ImageWithTitleModel[size];
        }
    };
}
