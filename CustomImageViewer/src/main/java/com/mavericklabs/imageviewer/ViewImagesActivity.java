package com.mavericklabs.imageviewer;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.mavericklabs.imageviewer.adapters.ViewImageAdapter;
import com.mavericklabs.imageviewer.callbacks.ViewImageAdapterListener;
import com.mavericklabs.imageviewer.constants.IndicatorType;
import com.mavericklabs.imageviewer.models.ImageModel;
import com.mavericklabs.imageviewer.models.ImageWithTitleModel;
import com.mavericklabs.imageviewer.pagerIndicator.CirclePagerIndicatorDecoration;
import com.mavericklabs.imageviewer.pagerIndicator.LinePagerIndicatorDecoration;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Prashant Kashetti on 15/12/18.
 */
public class ViewImagesActivity extends AppCompatActivity implements View.OnClickListener, ViewImageAdapterListener {
    public static final String IMAGE_URLS = "image_urls";
    public static final String IMAGE_WITH_TITLES = "image_with_titles";
    public static final String INDICATOR_TYPE = "indicator_type";
    public static final String IS_FOR_EDIT = "is_for_edit";
    public static final String PLACE_HOLDER_IMAGE = "place_holder_image";
    public static final String TITLE = "title";
    public static final String TITLE_ID = "title_id";
    public static final String IS_FOR_SELECT = "is_for_select";
    public static final String SELECT_BUTTON_NAME = "select_button_name";
    private final int PERMISSION_REQUEST_CODE = 200;
    private final int CHECK_SETTINGS = 300;
    private Button btnChangeDocument, btnSelectDocument;
    private ImageModel currentImageModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Fresco.hasBeenInitialized())
            Fresco.initialize(this);
        setContentView(R.layout.activity_recycler_view);
        if (getIntent().hasExtra(TITLE)) {
            setTitle(getIntent().getStringExtra(TITLE));
        } else if (getIntent().hasExtra(TITLE_ID)) {
            setTitle(getIntent().getIntExtra(TITLE_ID, 0));
        } else {
            setTitle("");
        }
        boolean isForEdit = getIntent().getBooleanExtra(IS_FOR_EDIT, false);
        boolean isForSelect = getIntent().getBooleanExtra(IS_FOR_SELECT, false);
        btnChangeDocument = findViewById(R.id.btnChangeDocument);
        btnSelectDocument = findViewById(R.id.btnSelectDocument);
        if (isForEdit) {
            ViewCompat.setBackgroundTintList(btnChangeDocument, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorAccent)));
            btnChangeDocument.setVisibility(View.VISIBLE);
            btnChangeDocument.setOnClickListener(this);
        } else
            btnChangeDocument.setVisibility(View.GONE);
        if (isForSelect) {
            ViewCompat.setBackgroundTintList(btnSelectDocument, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorAccent)));
            btnSelectDocument.setVisibility(View.VISIBLE);
            btnSelectDocument.setOnClickListener(this);
            int selectButtonName = getIntent().getIntExtra(SELECT_BUTTON_NAME, R.string.select_document);
            btnSelectDocument.setText(selectButtonName);
        } else
            btnSelectDocument.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermissions() {
        requestPermissions(new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE
        }, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PERMISSION_REQUEST_CODE == requestCode) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                final RecyclerView recyclerView = findViewById(R.id.recycler);
                recyclerView.setLayoutManager(
                        new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                if (getIntent().hasExtra(IMAGE_URLS))
                    configRecyclerForUrls();
                else
                    configRecyclerForImageWithTitles();
            } else {
                onAppPermissionDenied();
            }
        }
    }

    void onAppPermissionDenied() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.permission_denied_title)
                .setMessage(R.string.permission_denied_message)
                .setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, CHECK_SETTINGS);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                })
                .setCancelable(false)
                .create();
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private void configRecyclerForUrls() {
        ArrayList<String> images = getIntent().getStringArrayListExtra(IMAGE_URLS);
        IndicatorType type = (IndicatorType) getIntent().getSerializableExtra(INDICATOR_TYPE);
        Integer placeHolderImageId = null;
        if (getIntent().hasExtra(PLACE_HOLDER_IMAGE)) {
            placeHolderImageId = getIntent().getIntExtra(PLACE_HOLDER_IMAGE, -1);
        }
        ArrayList<ImageModel> imageModels = new ArrayList<>();
        for (String url : images) {
            Uri uri;
            if (url.startsWith("http")) {
                uri = Uri.parse(url);
            } else {
                uri = Uri.fromFile(new File(url));
                ImagePipeline imagePipeline = Fresco.getImagePipeline();
                imagePipeline.evictFromCache(uri);
            }
            imageModels.add(new ImageModel(uri));
        }
        RecyclerView recycler = findViewById(R.id.recycler);
        if (type == IndicatorType.LINE)
            recycler.addItemDecoration(new LinePagerIndicatorDecoration());
        else
            recycler.addItemDecoration(new CirclePagerIndicatorDecoration());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycler.setLayoutManager(linearLayoutManager);
        recycler.setAdapter(new ViewImageAdapter(imageModels, placeHolderImageId, null));
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recycler);
    }

    private void configRecyclerForImageWithTitles() {
        ArrayList<ImageWithTitleModel> images = getIntent().getParcelableArrayListExtra(IMAGE_WITH_TITLES);
        IndicatorType type = (IndicatorType) getIntent().getSerializableExtra(INDICATOR_TYPE);
        Integer placeHolderImageId = null;
        if (getIntent().hasExtra(PLACE_HOLDER_IMAGE)) {
            placeHolderImageId = getIntent().getIntExtra(PLACE_HOLDER_IMAGE, -1);
        }
        final ArrayList<ImageModel> imageModels = new ArrayList<>();
        for (ImageWithTitleModel model : images) {
            Uri uri;
            if (model.getUrl().startsWith("http")) {
                uri = Uri.parse(model.getUrl());
            } else {
                uri = Uri.fromFile(new File(model.getUrl()));
                ImagePipeline imagePipeline = Fresco.getImagePipeline();
                imagePipeline.evictFromCache(uri);
            }
            ImageModel imageModel = new ImageModel(model.getId(), model.getTitle(), uri);
            imageModels.add(imageModel);
        }
        RecyclerView recycler = findViewById(R.id.recycler);
        if (type == IndicatorType.LINE)
            recycler.addItemDecoration(new LinePagerIndicatorDecoration());
        else
            recycler.addItemDecoration(new CirclePagerIndicatorDecoration());
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycler.setLayoutManager(linearLayoutManager);
        recycler.setAdapter(new ViewImageAdapter(imageModels, placeHolderImageId, this));
        final LinearSnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recycler);
        currentImageModel = imageModels.get(0);
        setTitle(currentImageModel.getTitle());
        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    View centerView = snapHelper.findSnapView(linearLayoutManager);
                    int pos = linearLayoutManager.getPosition(centerView);
                    currentImageModel = imageModels.get(pos);
                    setTitle(currentImageModel.getTitle());
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnChangeDocument) {
            setResult(RESULT_OK);
            finish();
        } else if (id == R.id.btnSelectDocument) {
            Intent intent = new Intent();
            intent.putExtra(ImageViewer.SELECTED_IMAGE_ID, currentImageModel.getId());
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
