package com.mavericklabs.imageviewer.adapters;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.mavericklabs.imageviewer.R;
import com.mavericklabs.imageviewer.callbacks.ViewImageAdapterListener;
import com.mavericklabs.imageviewer.drawables.CircleProgressBarDrawable;
import com.mavericklabs.imageviewer.models.ImageModel;
import com.mavericklabs.imageviewer.zoomable.ZoomableDraweeView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Prashant Kashetti on 15/12/18.
 */
public class ViewImageAdapter extends RecyclerView.Adapter<ViewImageAdapter.ViewImageHolder> {
    private ArrayList<ImageModel> imageModels;
    private Integer placeHolderImageId;
    private ViewImageAdapterListener listener;

    public ViewImageAdapter(ArrayList<ImageModel> imageModels, @Nullable Integer placeHolderImageId, @Nullable ViewImageAdapterListener listener) {
        this.imageModels = imageModels;
        this.placeHolderImageId = placeHolderImageId;
        this.listener = listener;
        setHasStableIds(true);
    }

    @Override
    public ViewImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_image_item_row, parent, false);
        return new ViewImageHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewImageHolder holder, int position) {
        ImageModel imageModel = imageModels.get(position);
        final Uri uri = imageModel.getUri();
        ZoomableDraweeView view = holder.zoomableDraweeView;
        String mimeType = getMimeType(view.getContext(), uri);
        if (mimeType.equals("pdf")) {
            ViewCompat.setBackgroundTintList(holder.btnOpenPdf, ColorStateList.valueOf(ContextCompat.getColor(holder.btnOpenPdf.getContext(), R.color.colorAccent)));
            view.setVisibility(View.GONE);
            holder.llOpenPdf.setVisibility(View.VISIBLE);
            holder.btnOpenPdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        openPdf(view.getContext(), new File(uri.getPath()));
                    } catch (IllegalArgumentException e) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
                        view.getContext().startActivity(browserIntent);
                    }
                }
            });
        } else {
            holder.llOpenPdf.setVisibility(View.GONE);
            holder.btnOpenPdf.setOnClickListener(null);
            view.setVisibility(View.VISIBLE);
            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                    .setProgressiveRenderingEnabled(true)
                    .setRotationOptions(RotationOptions.autoRotate())
                    .build();
            DraweeController ctrl = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setTapToRetryEnabled(true)
                    .build();
            GenericDraweeHierarchy hierarchy = new GenericDraweeHierarchyBuilder(view.getResources())
                    .setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER)
                    .setPlaceholderImageScaleType(ScalingUtils.ScaleType.FIT_CENTER)
                    .setProgressBarImage(new ProgressBarDrawable())
                    .build();
            hierarchy.setProgressBarImage(new CircleProgressBarDrawable(holder.zoomableDraweeView.getContext()));
            if (placeHolderImageId != null) {
                hierarchy.setPlaceholderImage(placeHolderImageId);
            }
            view.setController(ctrl);
            view.setHierarchy(hierarchy);
        }
    }

    @Override
    public int getItemCount() {
        return imageModels.size();
    }

    @Override
    public long getItemId(int position) {
        return imageModels.get(position).hashCode();
    }

    private String getMimeType(Context context, Uri uri) {
        String extension;
        //Check uri format to avoid null
        if (uri.getScheme() != null && uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
        }
        return extension;
    }

    private void openPdf(Context context, File file) {
        Uri photoURI = FileProvider.getUriForFile(context,
                context.getPackageName() + ".provider",
                file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(photoURI, "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, R.string.no_pdf_app_found_dialog_message, Toast.LENGTH_LONG).show();
        }
    }

    static class ViewImageHolder extends RecyclerView.ViewHolder {
        private final ZoomableDraweeView zoomableDraweeView;
        private final LinearLayout llOpenPdf;
        private final Button btnOpenPdf;

        private ViewImageHolder(View itemView) {
            super(itemView);
            zoomableDraweeView = itemView.findViewById(R.id.my_image_view);
            llOpenPdf = itemView.findViewById(R.id.llOpenPdf);
            btnOpenPdf = itemView.findViewById(R.id.btnOpenPdf);
        }
    }
}
