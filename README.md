# Custom Image Viewer

This library is used to view images.This library is built on [Fresco](https://frescolib.org/docs/index.html) library. Following features are implemented

  - Zoom image using double tap as well as pinch
  - custom place holder
  - circular progress bar
  - Page indicator (line and circle)

# Requirements
  - Minimum API level 16

# Install
Download via **Gradle**:
```
implementation 'com.mavericklabs:CustomImageViewer:1.0.0'
```

or **Maven**:

```
<dependency>
  <groupId>com.mavericklabs</groupId>
  <artifactId>CustomImageViewer</artifactId>
  <version>1.0.0</version>
  <type>pom</type>
</dependency>
```

# Usage

```
new ImageViewer.Builder(this)
                .urls(urls)
                .indicatorType(IndicatorType.LINE)
                .show();
```

here **urls** is **``ArrayList<String>``**.By default indicator type is **``IndicatorType.CIRCLE``**

# License

```
Copyright (C) 2019 mavericklabs.in

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```